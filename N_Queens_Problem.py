import sys

N=int(sys.argv[1])

a = [[0 for x in range((N+1))] for y in range((N+1))] 

zz=0;

def tally(i,j):
    global zz
    zz=zz+1;
    print "Soln.",zz,":";
    for m in range(1,(N+1)):
        for n in range(1,(N+1)):
            if(a[n][m]==1):
                sys.stdout.write(str(n)+",")
                sys.stdout.flush()
                #print "(",n,",",m,")";
    print i
    #print "(",i,",",j,")";

def suc(m,n):
    x=[0,1,1,0,1,1,-1,0,0,-1,-1,-1,-1,1,1,-1];
    for i in range(0,16,2):
        k=m;
        l=n;
        while (k>=1) and (k<=N) and (l>=1) and (l<=N):
            if(a[k][l]==1):
                return 0;
            k=k+x[i];
            l=l+x[i+1];
    return 1;

def place(i,j):
    if(suc(i,j)):
        if(j==N):
            tally(i,j);
        else:
            a[i][j]=1;
            for k in range(1,(N+1)):
                place(k,j+1);
            a[i][j]=0;

place(0,0)