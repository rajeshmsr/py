import sys
import random
#import mod_puz_sol

if(len(sys.argv)>=3 and sys.argv[2].isdigit()):
    fd=int(sys.argv[2])
else:
    fd=0

if(len(sys.argv)>=2 and sys.argv[1].isdigit()):
    N=int(sys.argv[1])
else:
    N=3

a = [x+1 for x in range(0,N*N)]
b = ["=" for x in range(0,2*N*(N-1))]

c=["+","-","*"]

v = []

def randomize_list(listn,n):
    for y in range(n-1,fd,-1):
        r=random.randint(fd,y)
        t=listn[y]
        listn[y]=listn[r]
        listn[r]=t

randomize_list(a,N*N)

for i in range(0,2*N*(N-1)):
    r=random.randint(0,len(c)-1)
    b[i]=c[r]
    
for i in range(0,N):
    si=str(str(a[i*N]))
    for j in range(1,N):
        si=si+"\t"+b[i*(N-1)+j-1]+"\t"+str(a[i*N+j])
    v.append(eval(si))
    print si+"\t=\t"+str(v[-1])
    si="=" if(i==N-1) else b[N*(N-1)+i]
    for j in range(1,N):
        si=si+"\t\t"+("=" if(i==N-1) else b[(N+j)*(N-1)+i]) 
    print si

sj=""
for i in range(0,N):
    si=str(a[i])
    for j in range(1,N):
        si=si+"\t"+b[(N+i)*(N-1)+j-1]+"\t"+str(a[i+j*N])
    v.append(eval(si))
    sj=sj+str(v[-1])+"\t\t"
print sj

print b
print v

#a = [0 for x in range(0,N*N)]
#aa = [x+1 for x in range(0,N*N)]
#mod_puz_sol.place(set(aa),a,b,v,N,0)
